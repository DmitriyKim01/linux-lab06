#!/bin/bash
#Script 4
#Dmitriy Kim
#Sonic username: 2232931@sonic.dawsoncollege.qc.ca

source_path=$(pwd)
dirname=$(basename $source_path)
destination="$HOME/public_html/$dirname"

echo "Removing previous files in the $destination directory!"
rm -rf $destination

echo "Publishing $source_path to $destination"
cp -vir $source_path $destination

tree $destination

echo "Updating permissions: "

chmod -Rv a+r $destination

find $destination -type d -exec chmod -v a+x {}  \;



