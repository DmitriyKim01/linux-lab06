
#!/bin/bash
# Dmitriy Kim
# Script 2
# Sonic username: 2232931@sonci.dawsoncollege.qc.ca

#a.
BACKUP_FILE="labs_$(date +%F)_$(date +%T)"

#b.
mkdir -p ~/my_backup

#c.
cp -r ~/240 ~/my_backup/$BACKUP_FILE

# The question #i part:
STDOUT="$HOME/my_backup/$BACKUP_FILE/log_output.txt"
STDERR="$HOME/my_backup/$BACKUP_FILE/log_error.txt"
echo "" > $STDOUT
echo "" > $STDERR

#d.
cd ~/my_backup/$BACKUP_FILE
echo "The current working directory is: $HOME/my_backup/$BACKUP_FILE" >> $STDOUT

#e.
echo "Number of files and directories in $BACKUP_FILE: $(ls -a ~/my_backup/$BACKUP_FILE | wc -w | tee)" >> $STDOUT

#f.
mkdir ~/my_backup_links 2> $STDERR

#g.
Link_Path="link.to.lab_$(date +%F)_$(date +%T)"
ln -s ~/my_backup/$BACKUP_FILE ~/my_backup_links/$Link_Path

#h.
ls ~/my_backup_links/$Link_Path >> $STDOUT

#i.  1 - I have created a new variable for the soft link path: Link_Path. Used it in question #g and #h
#    2 - Yes we can. I have used it in the question #b
#    3 - Two variables STDOUT and STDERR are created in #c
#    4 - Modified ion questions: #d, #e, #f, #h
#    5 - Deleted the content of variables in the question #c


