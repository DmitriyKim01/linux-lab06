#!/bin/bash
#Dmitriy Kim
#2232931@sonic.dawsoncollege.qc.ca

#b
cp -ivr ~/skel/web/* ~/240/lab06/240-lab06-history

#c
tree ~/240/lab06/240-lab06-history

#d
PATH=$PATH:~/240/lab06
# I have added a new path (~/240/lab06) to the PATH variable in ~/.bashrc. This feature allows me to run the script anywhere
# from my local user environment. Basically, everytime I run a script bash checks the PATH variable in order to verify if 
# command exist
